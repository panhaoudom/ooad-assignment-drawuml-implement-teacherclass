import model.subject.AndroidSubject;
import model.subject.OOADSubject;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        // android subject
        AndroidSubject androidSubject = new AndroidSubject(1, "android", LocalDateTime.now());
        System.out.println("android subjectId : " + androidSubject.getSubjectId());
        System.out.println("android subjectName : " + androidSubject.getSubjectName());
        System.out.println("android studyTime : " + androidSubject.getStudyDatetime());
        androidSubject.doClassActivity();
        androidSubject.doAssignment();

        System.out.println("=============================================");

        // android subject
        OOADSubject OOADSubject = new OOADSubject(2, "OOAD", LocalDateTime.of(2022,10,02, 13, 30));
        System.out.println("OOAD subjectId : " + OOADSubject.getSubjectId());
        System.out.println("OOAD subjectName : " + OOADSubject.getSubjectName());
        System.out.println("OOAD studyTime : " + OOADSubject.getStudyDatetime());
        OOADSubject.doClassActivity();
        OOADSubject.doAssignment();
    }
}

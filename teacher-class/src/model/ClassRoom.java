package model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import model.subject.Subject;

@AllArgsConstructor
@NoArgsConstructor
public class ClassRoom {
    private Subject subject;
    private String roomNumber;
    private Integer studentAmount;

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getStudentAmount() {
        return studentAmount;
    }

    public void setStudentAmount(Integer studentAmount) {
        this.studentAmount = studentAmount;
    }
}

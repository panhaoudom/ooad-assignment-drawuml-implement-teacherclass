package model.teacher;

import model.ClassRoom;
import model.Gender;
import model.subject.Subject;

import java.util.List;

public class Teacher{
    private Integer id;
    private String firstName;
    private Gender gender;
    private List<ClassRoom> rooms;
    private List<Subject> subjects;
    private List<TeacherTask> tasks;
    private String phoneNumber;
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public List<TeacherTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<TeacherTask> tasks) {
        this.tasks = tasks;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}

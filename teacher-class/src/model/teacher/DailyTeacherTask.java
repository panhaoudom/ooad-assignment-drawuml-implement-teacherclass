package model.teacher;

public interface DailyTeacherTask {
    void prepareDocument();
    void prepareLesson();
    void prepareQuiz();
    void checkStudentAttendance();
    void signTeacherAttendance();
}

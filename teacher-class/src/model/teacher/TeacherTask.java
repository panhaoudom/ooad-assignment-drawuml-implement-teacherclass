package model.teacher;

public class TeacherTask implements DailyTeacherTask{
    protected Integer taskId;
    protected String taskName;
    protected String taskDescription;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    @Override
    public void prepareDocument() {
        System.out.println("prepareDocument");
    }

    @Override
    public void prepareLesson() {
        System.out.println("prepareLesson");
    }

    @Override
    public void prepareQuiz() {
        System.out.println("prepareQuiz");
    }

    @Override
    public void checkStudentAttendance() {
        System.out.println("checkStudentAttendance");
    }

    @Override
    public void signTeacherAttendance() {
        System.out.println("signTeacherAttendance");
    }
}

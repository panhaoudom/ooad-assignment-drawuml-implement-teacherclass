package model.subject;

import lombok.Data;

import java.time.LocalDateTime;

public abstract class Subject {

    protected Integer subjectId;

    protected String subjectName;

    public Integer getSubjectId() {
        return subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public LocalDateTime getStudyDatetime() {
        return studyDatetime;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public void setStudyDatetime(LocalDateTime studyDatetime) {
        this.studyDatetime = studyDatetime;
    }

    protected LocalDateTime studyDatetime;

    public abstract void doClassActivity();

    public abstract void doAssignment();
}

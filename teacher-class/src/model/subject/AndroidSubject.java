package model.subject;
import java.time.LocalDateTime;

public class AndroidSubject extends Subject{

    public AndroidSubject(Integer subjectId, String subjectName, LocalDateTime studyDatetime) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.studyDatetime = studyDatetime;
    }

    @Override
    public void doClassActivity() {
        System.out.println("do android class activity......!");
    }

    @Override
    public void doAssignment() {
        System.out.println("do android assignment......!");
    }
}

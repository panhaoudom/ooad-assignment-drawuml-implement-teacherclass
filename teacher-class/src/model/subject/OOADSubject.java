package model.subject;

import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
public class OOADSubject extends Subject{

    public OOADSubject(Integer subjectId, String subjectName, LocalDateTime studyDatetime) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.studyDatetime = studyDatetime;
    }

    @Override
    public void doClassActivity() {
        System.out.println("do OOAD class activity......!");
    }

    @Override
    public void doAssignment() {
        System.out.println("do OOAD assignment......!");
    }
}
